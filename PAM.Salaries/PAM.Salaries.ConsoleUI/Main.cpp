//Paul Mutimba
//Salaries

#include<conio.h>
#include<iostream>
#include <array>
#include <vector>


using namespace std;
struct EmployeeRecord
{
	int id;
	string firstname;
	string lastname;
	float payrate;
	float hours;
};


int main()
{

	int SIZE;
	cout << "Enter number of Employees: \n";
	cin >> SIZE;

	vector<EmployeeRecord> e(SIZE);
	string name;
	float grosspay;
	float totalgrosspay = 0.00f;

	for (int i = 0; i < SIZE; i++)
	{
		cout << "------------------------------\n";
		cout << "ID: \n";
		cin >> e[i].id;

		cout << "First Name: \n";
		cin >> e[i].firstname;

		cout << "Last Name: \n";
		cin >> e[i].lastname;

		cout << "Pay Rate: \n";
		cin >> e[i].payrate;

		cout << "Please Enter Hours: \n";
		cin >> e[i].hours;

	}
	for (int i = 0; i < SIZE; i++)
	{
		cout << "------------------------------\n";
		cout << "Employee " << i + 1 << ":\n";
		name = e[i].firstname + " " + e[i].lastname;
		float grosspay = e[i].payrate * e[i].hours;
		cout << "ID: " << e[i].id << "\n";
		cout << "Name: " << name << "\n";

		cout << "Gross Pay: " << grosspay << "\n\n";

		totalgrosspay += grosspay;

	}

	cout << "=============================\n";
	cout << "Total Gross Pay: " << totalgrosspay << ".\n";

	(void)_getch();
	return 0;
}